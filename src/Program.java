import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Program {

	public static void main(String[] args) throws IOException {
		String configFile = "config.txt";
		ArrayList<String> valueStrings = new ArrayList<String>();
		ValueMenu menu = new ValueMenu();
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		String currentDate = dateFormat.format(date);

		while (true) {

			menu.printValuesMenu();
			valueStrings = menu.get_stringValues();
			valueStrings.add(currentDate);

			ImageCustom image = new ImageCustom(configFile, valueStrings);

			if (!menu.get_okIsPressed()) {
				image.display();
				
			} else {
				menu.set_okIsPressed(false);
				image.save();
				image.updateCounter();
				BufferedImage imageToPrint = image.get_image();
				new Thread(new PrintActionListener(imageToPrint)).start();
			}
			menu.set_okIsPressed(false);
			menu.setPreviewIsPressed(false);
//			menu.showMenu();
			menu.set_stringValue(valueStrings);
		}
	}

}
