import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class ValueMenu {

	private ArrayList<String> stringValues;
	private boolean okIsPressed;
	private boolean previewIsPressed;

	private String[] labelString = { "Model", "Numar", "Convergeta stanga fata", "Convergeta dreapta fata",
			"Cadere stanga fata", "Cadere dreapta fata", "Fuga staga", "Fuga dreapta", "Dezaxare",
			"Convergeta stanga spate", "Convergeta dreapta spate", "Cadere stanga spate", "Cadere dreapta spate", };
	private String[] carModels = { "Alfa Romeo", "Audi", "BMW", "Cadillac", "Chevrolet", "Chrysler", "Citroen", "Dacia",
			"Daewoo", "Daihatsu", "Dodge", "Fiat", "Ford", "Honda", "Hyundai", "Infiniti", "Iveco","Jaguar", "Jeep", "Kia",
			"Land Rover", "Lexus", "Mazda", "Mercedes-Benz", "Mini", "Mitsubishi", "Nissan", "Opel", "Peugeot", "Rover",
			"Renault", "Saab", "Seat", "Skoda", "Smart", "Subaru", "Suzuki", "Toyota", "Volkswagen", "Volvo" };

	private int fieldNumbers = labelString.length;

	public ValueMenu() {
		okIsPressed = false;
		previewIsPressed = false;
		stringValues = new ArrayList<String>();
		for (int i = 0; i < fieldNumbers; i++) {
			stringValues.add("");
		}
	}

	public boolean get_previewIsPressed() {
		return previewIsPressed;
	}

	public boolean get_okIsPressed() {
		return okIsPressed;
	}

	public ArrayList<String> get_stringValues() {
		return stringValues;
	}

	public void printValuesMenu() {

		JTextField[] txtAllAverages;

		float fontSize = 30.0f;

		txtAllAverages = new JTextField[fieldNumbers];

		JPanel inputControls = new JPanel(new BorderLayout(5, 5));

		JPanel inputControlsLabels = new JPanel(new GridLayout(0, 1, 3, 3));
		JPanel inputControlsFields = new JPanel(new GridLayout(0, 1, 3, 3));
		inputControls.add(inputControlsLabels, BorderLayout.WEST);
		inputControls.add(inputControlsFields, BorderLayout.CENTER);

		JComboBox<String> comboBox = new JComboBox<>();
		for (int i = 0; i < carModels.length; i++) {
			comboBox.addItem(carModels[i]);
		}

		JLabel auxLabel = new JLabel(labelString[0]);
		auxLabel.setFont(auxLabel.getFont().deriveFont(fontSize));
		inputControlsLabels.add(auxLabel);
		inputControlsFields.add(comboBox);

		for (int i = 1; i < fieldNumbers; i++) {

			auxLabel = new JLabel(labelString[i]);
			auxLabel.setFont(auxLabel.getFont().deriveFont(fontSize));
			inputControlsLabels.add(auxLabel);
			JTextField field = new JTextField(10);
			field.setFont(field.getFont().deriveFont(fontSize));
			inputControlsFields.add(field);
			txtAllAverages[i] = field;
			txtAllAverages[i].setText(stringValues.get(i));
		}

		JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 2));

		JButton okButton = new JButton("Print");
		JButton resetButton = new JButton("Reset");
		JButton settingsButton = new JButton("Settings");
		JButton previewButton = new JButton("Preview");
		JButton testButton = new JButton("test");

		controls.add(settingsButton);
		controls.add(resetButton);
		controls.add(previewButton);
		controls.add(okButton);

		JPanel gui = new JPanel(new BorderLayout(10, 10));
		gui.setBorder(new TitledBorder("Valori"));
		gui.add(inputControls, BorderLayout.CENTER);
		gui.add(controls, BorderLayout.SOUTH);

		JFrame averageFrame = new JFrame("GEO-AND SERVICE");

		settingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					SettingsMenu settingsMenu = new SettingsMenu("config.txt");
				} catch (NumberFormatException | IOException e1) {
				}

			}
		});

		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBox.setSelectedIndex(0);
				for (int i = 1; i < fieldNumbers; i++) {
					txtAllAverages[i].setText("");
				}
			}
		});

		previewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (testButton) {
					stringValues.clear();
					stringValues.add(carModels[comboBox.getSelectedIndex()]);
					for (int i = 1; i < fieldNumbers; i++) {

						stringValues.add(txtAllAverages[i].getText());
					}
					previewIsPressed = true;
					averageFrame.setVisible(false);
					averageFrame.dispose();
					testButton.notify();
				}
			}
		});

		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				synchronized (testButton) {
					stringValues.clear();
					stringValues.add(carModels[comboBox.getSelectedIndex()]);
					for (int i = 1; i < fieldNumbers; i++) {
						stringValues.add(txtAllAverages[i].getText());
					}
					averageFrame.setVisible(false);
					okIsPressed = true;
					previewIsPressed = true;
					averageFrame.dispose();
					testButton.notify();
				}
			}
		});

		averageFrame.setContentPane(gui);
		averageFrame.pack();
		averageFrame.setLocationByPlatform(true);
		averageFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		averageFrame.setLocationRelativeTo(null);
		averageFrame.setVisible(true);

		synchronized (testButton) {
			try {
				testButton.wait();
				return;
			} catch (InterruptedException e1) {
			}
		}
	}

	public void set_stringValue(ArrayList<String> valueStrings) {
		stringValues = valueStrings;
	}

	public void set_okIsPressed(boolean b) {
		okIsPressed = b;		
	}

	public void setPreviewIsPressed(boolean b) {
		previewIsPressed = b;
	}
}
