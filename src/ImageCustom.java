import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

// clasa care pune valorile pe imagine, citeste si salveaza imaginea si updateaza datele din config
public class ImageCustom {

	private ArrayList<String> stringValues;
	private BufferedImage image;
	private boolean backIsPressed;
	private BufferedImage bimage;
	private String configFile;
	private String outputPath;
	private String inputPath;
	private int counterID;

	public ImageCustom(String aConfigFile, ArrayList<String> aStringValues) throws IOException {
		stringValues = aStringValues;
		configFile = aConfigFile;
		image = null;
		backIsPressed = false;
		bimage = null;

		getPaths();
		read();
		placeValues();
		scale();
	}

	public void read() throws IOException {
		image = ImageIO.read(new File(inputPath));
	}

	public void scale() {
		Image img = image.getScaledInstance(450, 600, Image.SCALE_FAST);
		bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();
	}

	private String getOutputFileString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String currentDate = dateFormat.format(date);

		return outputPath + "\\" + currentDate + "-" + Integer.toString(counterID) + "-" + stringValues.get(1) + ".png";
	}

	public void save() throws IOException {

		Image imgToSave = image.getScaledInstance(595, 842, Image.SCALE_FAST);

		BufferedImage bufferdImageToSave = new BufferedImage(imgToSave.getWidth(null), imgToSave.getHeight(null),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D bGrToSave = bufferdImageToSave.createGraphics();
		bGrToSave.drawImage(imgToSave, 0, 0, null);
		bGrToSave.dispose();
		ImageIO.write(bufferdImageToSave, "PNG", new File(getOutputFileString()));
	}

	public BufferedImage get_image() {
		return image;
	}

	public String formatValue(String value) {
		if (value.length() == 0)
			return "";
		boolean negativeValue = false;
		if (value.length() >= 2 && value.charAt(0) == '-') {
			negativeValue = true;
			value = value.substring(1);
		}
		value = "000" + value;
		value = value.substring(value.length() - 3);
		String formatedString = String.format("%s°%s'", value.substring(0, value.length() - 2),
				value.substring(value.length() - 2));
		if (negativeValue)
			formatedString = "-" + formatedString;
		return formatedString;
	}

	private int degreeToInt(String value) {

		int aux = 1;
		if (value.length() >= 2 && value.charAt(0) == '-') {
			aux = -1;
			value = value.substring(1);
		}
		value = "000" + value;
		value = value.substring(value.length() - 3);
		int degree = Integer.parseInt(value.substring(0, value.length() - 2));
		int minutes = Integer.parseInt(value.substring(value.length() - 2));
		return aux * (degree * 60 + minutes);

	}

	private String getTotalToe(String a, String b) {
		if (a.length() == 0 || b.length() == 0)
			return "";
		int sum = degreeToInt(a) + degreeToInt(b);

		int minutes = sum % 60;
		int degrees = sum / 60;
		if (degrees < 0) {
			degrees *= -1;
		}
		if (minutes < 0) {
			minutes *= -1;
		}
		if (sum < 0) {
			return String.format("-%d%02d", degrees, minutes);
		}
		return String.format("%d%02d", degrees, minutes);
	}

	public void placeValues() {
		Graphics g = image.getGraphics();
		Font font = new Font("Arial", Font.PLAIN, 70);
		g.setFont(font);
		g.setColor(Color.BLACK);

		g.drawString(stringValues.get(0).toUpperCase(), 1750, 180); // Marca
		g.drawString(stringValues.get(1).toUpperCase(), 2000, 342);// numar
		g.drawString(formatValue(stringValues.get(2)), 490, 1000); // conv s f
		g.drawString(formatValue(stringValues.get(3)), 1908, 1000); // conv d f
		g.drawString(formatValue(stringValues.get(4)), 490, 1300); // cad s f
		g.drawString(formatValue(stringValues.get(5)), 1908, 1300); // cad d f
		g.drawString(formatValue(stringValues.get(6)), 490, 1600); // f s
		g.drawString(formatValue(stringValues.get(7)), 1908, 1600); // f d
		g.drawString(formatValue(stringValues.get(8)), 1100, 1800); // dezax
		g.drawString(formatValue(stringValues.get(9)), 490, 2300); // conv s s
		g.drawString(formatValue(stringValues.get(10)), 1908, 2300); // conv d s
		g.drawString(formatValue(stringValues.get(11)), 490, 2700); // cad s s
		g.drawString(formatValue(stringValues.get(12)), 1908, 2700); // cad d s
		g.drawString(stringValues.get(13), 1700, 500); // data
		g.drawString(formatValue(getTotalToe(stringValues.get(2), stringValues.get(3))), 1100, 1413); // totatl
																										// conv
																										// f
		g.drawString(formatValue(getTotalToe(stringValues.get(9), stringValues.get(10))), 1100, 2640); // total
																										// conv
																										// s
		g.drawString(Integer.toString(counterID), 2130, 104); // Nr deviz

		g.dispose();
	}

	public boolean get_backIsPressed() {
		return backIsPressed;
	}

	public void display() throws IOException {

		JFrame frame = new JFrame("GEO-AND SERVICE");
		JPanel panel = new JPanel(new BorderLayout(10, 10));
		ImageIcon imageToDisplay = new ImageIcon(bimage);
		JLabel label = new JLabel(imageToDisplay);
		JButton printButton = new JButton("Print");
		JButton backButton = new JButton("Back");
		JButton buttonPressed = new JButton();

		backButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				synchronized (buttonPressed) {
					frame.setVisible(false);
					frame.dispose();
					backIsPressed = true;
					buttonPressed.notify();
				}
			}
		});

		printButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				synchronized (buttonPressed) {
					try {
						save();
						updateCounter();
					} catch (IOException e) {
					}

					new Thread(new PrintActionListener(image)).start();
					frame.setVisible(false);
					frame.dispose();
					buttonPressed.notify();
				}
			}
		});
		JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 2));
		frame.setSize(bimage.getWidth(), bimage.getHeight() + 90);
		panel.add(label);
		controls.add(backButton);
		controls.add(printButton);
		panel.add(controls, BorderLayout.SOUTH);
		frame.add(panel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		synchronized (buttonPressed) {
			try {
				buttonPressed.wait();
			} catch (InterruptedException e1) {
			}
		}
	}

	public void updateCounter() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));

		counterID++;
		bw.write(Integer.toString(counterID));
		bw.write(System.getProperty("line.separator"));
		bw.write(inputPath);
		bw.write(System.getProperty("line.separator"));
		bw.write(outputPath);

		bw.close();

	}

	private void getPaths() throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(configFile));

		counterID = Integer.parseInt(br.readLine());
		inputPath = br.readLine();
		outputPath = br.readLine();
		br.close();
	}

}
